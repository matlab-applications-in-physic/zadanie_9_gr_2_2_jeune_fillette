% MatLab applications in physics
% Agata Kozio�


clear
clc

% opening file
plik=fopen('z3_1350V_x20.dat','r');
infoOPliku=dir('z3_1350V_x20.dat');
rozmar=infoOPliku.bytes; % check file size
ilosc=zeros(1,256);
porcja=50e6;

% reading file in parts
% couting number of each value
for k=1:fix(rozmiar/porcja)
    dane=fread(plik, porcja, 'uint8');
    for l=1:porcja
        ilosc(dane(l))=ilosc(dane(l))+1
    end
end

dane=fread(plik, porcja, 'uint8');
for l=1:(rozmiar-fix(rozmiar/porcja))
    ilosc(dane(l))=ilosc(dane(l))+1
end


fclose(plik);

% saving results as pdf file
saveas(plot(ilosc), 'z3_1350V_x20.dat_histogram.pdf');
saveas(title('Histogram'), 'z3_1350V_x20.dat_histogram.pdf');
saveas(xlabel('Wysokosc sygnalu'), 'z3_1350V_x20.dat_histogram.pdf');
saveas(ylabel('Czestosc wystepowania'), 'z3_1350V_x20.dat_histogram.pdf');

% searching noise level
poziomSzumu = noiseLevel(ilosc);
fprintf('Poziom szumu wynosi %d \n', noiseLevel);
